import definitions from './definitions'
import auth from './auth'
import users from './users'
import playlists from './playlists'
import playlistSongs from './playlist-songs'
import songs from './songs'

export const types = `
  ${auth.types}

  ${users.types}

  ${playlists.types}

  ${playlistSongs.types}

  ${songs.types}
`

export const queries = `
  type Query {
    ${auth.queries}

    ${users.queries}

    ${playlists.queries}

    ${playlistSongs.queries}

    ${songs.queries}
  }
`

export const mutations = `
  type Mutation {
    ${auth.mutations}

    ${users.mutations}

    ${playlists.mutations}

    ${playlistSongs.mutations}

    ${songs.mutations}
  }
`

export default `
  ${definitions}

  ${types}

  ${queries}

  ${mutations}
`
