export const types = `
  input SourcesInput {
    youtubeId: String
  }

  type Sources {
    youtubeId: ID
  }

  type Song {
    id: ID!
    creatorId: ID!
    name: String!
    sources: Sources
    createdAt: Date!
    updatedAt: Date!
    deletedAt: Date
  }

  type Songs {
    items: [Song]!
    count: Int!
    total: Int!
  }

  input SongsFilterBy {
    creatorId: ID
    playlistId: ID
  }

  enum SongsSortByProps {
    creatorId
    name
    createdAt
    updatedAt
  }

  input SongsSortBy {
    field: SongsSortByProps!
    direction: SortDirection = ASC
  }
`

export const queries = `
  song(
    id: ID!
  ): Song

  songs(
    filterBy: SongsFilterBy
    from: Int = 0
    limit: Int = 10
    sortBy: [SongsSortBy!]
  ): Songs
`

export const mutations = `
  createSong(
    name: String!
    sources: SourcesInput = {}
  ): Song @auth

  updateSong(
    id: ID!
    name: String
    sources: SourcesInput = {}
  ): Song @auth

  deleteSong(
    id: ID!
  ): Song @auth
`

export default {
  types,
  queries,
  mutations,
}
