export const types = `
  type Auth {
    token: String!
    googleToken: String!
  }
`

export const queries = `

`

export const mutations = `
  login(
    code: String!
    redirectUrl: String!,
  ): Auth
`

export default {
  types,
  queries,
  mutations,
}
