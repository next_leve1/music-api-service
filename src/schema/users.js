export const types = `
  type User {
    id: ID!
    name: String
    email: String!
    createdAt: Date!
    updatedAt: Date!
    deletedAt: Date
  }
`

export const queries = `
  me(id: String): User @auth
`

export const mutations = `
  updateUser(name: String): User @auth
  deleteUser: User @auth
`

export default {
  types,
  queries,
  mutations,
}
