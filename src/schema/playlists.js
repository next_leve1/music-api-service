export const types = `
  type Playlist {
    id: ID!
    creatorId: ID!
    name: String!
    createdAt: Date!
    updatedAt: Date!
    deletedAt: Date
  }

  type Playlists {
    items: [Playlist]!
    total: Int!
  }
`

export const queries = `
  playlist(
    id: ID!
  ): Playlist!

  playlists(
    limit: Int
    offset: Int
  ): Playlists!
`

export const mutations = `
  createPlaylist(
    name: String!
  ): Playlist! @auth

  updatePlaylist(
    id: ID!
  ): Playlist!

  deletePlaylist(
    id: ID!
  ): Playlist!
`

export default {
  types,
  queries,
  mutations,
}
