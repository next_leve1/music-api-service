export default `
  directive @auth on OBJECT | FIELD_DEFINITION

  scalar Date

  enum SortDirection {
    ASC
    DESC
  }
`
