export const types = `
  type PlaylistSong {
    id: ID!
    playlistId: ID!
    songId: ID!
    createdAt: Date!
    updatedAt: Date!
    deletedAt: Date
  }
`

export const queries = `
  playlistSongs(
    playlistId: ID!
    limit: Int
    offset: Int
  ): Songs!
`

export const mutations = `
  createPlaylistSong(
    playlistId: ID!
    songId: ID!
  ): PlaylistSong

  deletePlaylistSong(
    playlistId: ID!
    songId: ID!
  ): PlaylistSong
`

export default {
  types,
  queries,
  mutations,
}
