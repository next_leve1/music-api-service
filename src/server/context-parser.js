import log from '@log'

export default ({
  event: {
    headers: { token },
  },
}) => {
  try {
    const contextData = { token }

    return contextData
  } catch (error) {
    throw error
  }
}
