import { ApolloServer } from 'apollo-server-lambda'
import { typeDefs, resolvers, schemaDirectives } from '../modules'
import contextParser from './context-parser'

export const apolloServer = new ApolloServer({
  typeDefs,
  resolvers,
  schemaDirectives,
  context: contextParser,
  // formatError,
})

export default {
  typeDefs,
  resolvers,
  schemaDirectives,
  context: contextParser,
  apolloServer,
}
