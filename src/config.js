import dotenv from 'dotenv'
import serverlessConfig from '../serverless-config.json'

dotenv.config()

const getHost = () => {
  if (process.env.NODE_ENV === 'production') {
    return null
  }

  return 'http://localhost'
}

const getPort = () => serverlessConfig[process.env.NODE_ENV].port

const createUrl = (host, port) => {
  if (port) {
    return `${host}:${port}`
  }

  return host
}

const host = getHost()
const port = getPort()
const url = createUrl(host, port)

const base = {
  host,
  port,
  log: process.env.LOG === 'off' ? false : true,
  url,
  googleClientId: process.env.GOOGLE_CLIENT_ID,
  googleClientSecred: process.env.GOOGLE_CLIENT_SECRED,
  secret: process.env.SECRET,
}

export const configs = {
  development: {
    ...base,
    interfaceHost: 'http://localhost',
    interfacePort: 3000,
    database: {
      username: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD,
      database: 'music_development',
      host: '127.0.0.1',
      dialect: 'postgres',
      logging: false,
      operatorsAliases: false,
    },
  },
  test: {
    ...base,
    database: {
      username: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD,
      database: 'music_test',
      host: '127.0.0.1',
      dialect: 'postgres',
      logging: false,
      operatorsAliases: false,
    },
  },
  production: {
    ...base,
    database: {
      username: process.env.REMOTE_DB_USERNAME,
      password: process.env.REMOTE_DB_PASSWORD,
      database: 'musicc',
      host: 'musicc.cb1oov3aphjy.us-east-2.rds.amazonaws.com',
      dialect: 'postgres',
      operatorsAliases: false,
    },
  },
}

const env = process.env.NODE_ENV || 'development'

const { [env]: config } = configs

export default config
