import { create } from 'axios'

import config from '@/config'

export const baseURL = 'https://www.googleapis.com/'

const request = create({
  baseURL,
})

export const createTokenSettings = {
  path: '/oauth2/v4/token',
  headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
  params: ({ code, redirectUrl }) => ({
    code,
    client_id: config.googleClientId,
    client_secret: config.googleClientSecred,
    grant_type: 'authorization_code',
    redirect_uri: redirectUrl,
  }),
}

export const createToken = async ({ code, redirectUrl }) => {
  try {
    const {
      data: { access_token, refresh_token },
    } = await request({
      method: 'POST',
      url: createTokenSettings.path,
      headers: createTokenSettings.headers,
      params: createTokenSettings.params({ code, redirectUrl }),
    })

    setToken(access_token)

    return { token: access_token, refreshToken: refresh_token }
  } catch (error) {
    throw error
  }
}

export const setToken = (token) => {
  request.defaults.headers.common.Authorization = `Bearer ${token}`
}

export const getAccountSettings = {
  url: 'https://people.googleapis.com',
  path: '/v1/people/me',
  params: {
    personFields: 'emailAddresses',
  },
}

export const getAccount = async () => {
  try {
    const {
      data: {
        emailAddresses: [{ value: email }],
      },
    } = await request(`${getAccountSettings.url}${getAccountSettings.path}`, {
      params: getAccountSettings.params,
    })

    return { email }
  } catch (error) {
    throw error
  }
}

export default {
  createTokenSettings,
  createToken,
  setToken,
  getAccountSettings,
  getAccount,
}
