import { GraphQLScalarType, GraphQLNonNull, defaultFieldResolver } from 'graphql'
import { SchemaDirectiveVisitor } from 'graphql-tools'
import errors from '@errors'
// import { defaultFieldResolver } from 'graphql'
// import tokens from '@/services/tokens'
// import errors from '@errors'
// import log from '@log'

// https://github.com/apollographql/graphql-tools/issues/858

export class ValidationType extends GraphQLScalarType {
  getName() {
    return 'Length'
  }

  validate(value) {
    console.log('validate', value)

    if (typeof value === 'string' && value.length < 5) {
      throw new errors.ValidationError({ prop: 'inv' })
    }
  }

  constructor(type, constraint) {
    super({
      name: `IsVal`,
      description: 'Scalar type wrapper for input validation',

      /**
       * Server -> Client
       */
      serialize(value) {
        return type.serialize(value)
      },

      /**
       * Client (Variable) -> Server
       */
      parseValue(value) {
        const parsedValue = type.parseValue(value)

        this.validate(parsedValue)

        return parsedValue
      },

      /**
       * Client (Param) -> Server
       */
      parseLiteral(valueNode, variables) {
        const parsedValue = type.parseLiteral(valueNode, variables)

        this.validate(parsedValue)

        return parsedValue
      },
    })
  }
}

export class LengthConstraint {
  constructor(args) {
    this.args = args
  }

  getName() {
    return 'Length'
  }

  validate(value) {
    console.log('validate', value)

    if (typeof value === 'string' && value.length < 5) {
      throw new errors.ValidationError({ prop: 'inv' })
    }
  }

  // getCompatibleScalarKinds() {
  //   return [Kind.STRING]
  // }
}

// https://www.apollographql.com/docs/apollo-server/features/errors.html

export default class ValidationDirective extends SchemaDirectiveVisitor {
  visitInputObject(objectType) {
    const fields = objectType.getFields()

    Object.keys(fields).forEach((fieldName) => {
      const field = fields[fieldName]

      if (field.type instanceof GraphQLScalarType) {
        field.type = new ValidationType(field.type)
      } else if (
        field.type instanceof GraphQLNonNull &&
        field.type.ofType instanceof GraphQLScalarType
      ) {
        field.type = new GraphQLNonNull(new ValidationType(field.type.ofType))
      } else {
        throw new Error(`Type ${field.type} cannot be validated. Only scalars are accepted`)
      }
    })
  }

  // visitInputFieldDefinition(field) {
  //   if (field.type instanceof GraphQLScalarType) {
  //     field.type = new ValidationType(field.type, new LengthConstraint())
  //   } else if (
  //     field.type instanceof GraphQLNonNull &&
  //     field.type.ofType instanceof GraphQLScalarType
  //   ) {
  //     field.type = new GraphQLNonNull(new ValidationType(field.type.ofType, new LengthConstraint()))
  //   } else {
  //     throw new Error(`Type ${field.type} cannot be validated. Only scalars are accepted`)
  //   }
  // }

  // visitInputObject(fields, details) {
  //   console.log('visitInputObject', fields, details)
  // }

  // visitArgumentDefinition(fields, details) {
  //   console.log('visitArgumentDefinition', fields, details)
  // }

  // serialize(value) {
  //   console.log('serialize', value)

  //   return value
  // }

  // parseValue(value) {
  //   console.log('parseValue', value)

  //   return value
  // }

  // parseLiteral(valueNode) {
  //   console.log('parseLiteral', valueNode)

  //   return valueNode
  // }

  // visitObject(type) {
  //   type._requiredAuth = true

  //   this.ensureFieldsWrapped(type)
  // }

  // visitFieldDefinition(field, details) {
  //   field._requiredAuth = true

  //   this.ensureFieldsWrapped(details.objectType)
  // }

  // ensureFieldsWrapped(objectType) {
  //   if (objectType._authFieldsWrapped) return

  //   objectType._authFieldsWrapped = true

  //   const fields = objectType.getFields()

  //   Object.keys(fields).forEach((fieldName) => {
  //     const field = fields[fieldName]

  //     const { resolve = defaultFieldResolver } = field

  //     field.resolve = async (...args) => {
  //       const requiredAuth = field._requiredAuth || objectType._requiredAuth

  //       if (!requiredAuth) {
  //         return resolve.apply(this, args)
  //       }

  //       return resolve.apply(this, args)
  //     }
  //   })
  // }
}
