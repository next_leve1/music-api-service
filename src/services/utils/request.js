import { create } from 'axios'

export const baseURL = 'https://www.googleapis.com/'

export default create({
  baseURL,
})
