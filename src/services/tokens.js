import jwt from 'jsonwebtoken'
import config from '../config'

export const create = (payload = {}) => jwt.sign(payload, config.secret)

export const check = (token) => jwt.verify(token, config.secret)

export default {
  create,
  check,
}
