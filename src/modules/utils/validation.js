import errors from '@errors'

export default (validation) => (handler) => async (...args) => {
  const invalid = await validation(...args)

  if (invalid) {
    throw new errors.ValidationError(invalid)
  }

  return await handler(...args)
}
