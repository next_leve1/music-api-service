import { AuthenticationError } from 'apollo-server-lambda'

import db from '@/db'

export const types = `
  type Playlist {
    _id: ID!
    _ownerId: ID!
    name: String!
    createdAt: Date!
    updatedAt: Date!
    deletedAt: Date
  }

  type Playlists {
    items: [Playlist]!
    total: Int!
  }
`

export const queries = `
  playlist(
    _id: ID!
  ): Playlist!

  playlists(
    limit: Int
    offset: Int
  ): Playlists!
`

export const mutations = `
  createPlaylist(
    name: String!
  ): Playlist! @auth

  updatePlaylist(
    _id: ID!
    name: String
  ): Playlist! @auth

  removePlaylist(
    _id: ID!
  ): Playlist @auth
`

export const createHandler = async (parent, { name }, { _id }) => {
  try {
    const playlist = await db.playlists.insert({
      _ownerId: _id,
      name,
    })

    return playlist
  } catch (error) {
    throw error
  }
}

export const getHandler = async (parent, { _id }) => {
  try {
    const playlist = await db.playlists.get(_id)

    return playlist
  } catch (error) {
    throw error
  }
}

export const updateHandler = async (parent, { _id, name }, { _ownerId }) => {
  try {
    if (!(await db.playlists.exists({ _ownerId, _id }))) {
      throw new AuthenticationError('Playlist is not exists')
    }

    const playlist = await db.playlists.update(_id, { name })

    return playlist
  } catch (error) {
    throw error
  }
}

export const removeHandler = async (parent, { _id }, { _ownerId }) => {
  try {
    if (!(await db.playlists.exists({ _ownerId, _id }))) {
      throw new AuthenticationError('Playlist is not exists')
    }

    await db.playlists.remove(_id)
  } catch (error) {
    throw error
  }
}
