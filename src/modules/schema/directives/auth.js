import { SchemaDirectiveVisitor } from 'graphql-tools'
import { defaultFieldResolver } from 'graphql'
import { AuthenticationError } from 'apollo-server-lambda'

import tokens from '@/services/tokens'

const verify = (token = '') => {
  try {
    const payload = tokens.check(token)

    return payload
  } catch (error) {
    throw new AuthenticationError('Invalid token')
  }
}

export default class AuthDirective extends SchemaDirectiveVisitor {
  visitObject(type) {
    type._requiredAuth = true

    this.ensureFieldsWrapped(type)
  }

  visitFieldDefinition(field, details) {
    field._requiredAuth = true

    this.ensureFieldsWrapped(details.objectType)
  }

  ensureFieldsWrapped(objectType) {
    if (objectType._authFieldsWrapped) return

    objectType._authFieldsWrapped = true

    const fields = objectType.getFields()

    Object.keys(fields).forEach((fieldName) => {
      const field = fields[fieldName]

      const { resolve = defaultFieldResolver } = field

      field.resolve = async (parent, params, context, info) => {
        const requiredAuth = field._requiredAuth || objectType._requiredAuth

        if (!requiredAuth) {
          return resolve.apply(this, [parent, params, context, info])
        }

        const { token, ...nextContextBase } = context

        const payload = verify(token)

        const nextContext = {
          ...nextContextBase,
          ...payload,
        }

        return resolve.apply(this, [parent, params, nextContext, info])
      }
    })
  }
}
