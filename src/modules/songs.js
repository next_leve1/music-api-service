import db from '@/db'
import validations from '@/validations'
import validation from './utils/validation'

export const types = `
  input SourcesInput {
    youtubeId: String
  }

  type Sources {
    youtubeId: ID
  }

  type Song {
    id: ID!
    creatorId: ID!
    name: String!
    sources: Sources
    createdAt: Date!
    updatedAt: Date!
    deletedAt: Date
  }

  type Songs {
    items: [Song]!
    count: Int!
    total: Int!
  }

  input SongsFilterBy {
    creatorId: ID
    playlistId: ID
  }

  enum SongsSortByProps {
    creatorId
    name
    createdAt
    updatedAt
  }

  input SongsSortBy {
    field: SongsSortByProps!
    direction: SortDirection = ASC
  }
`

export const queries = `
  song(
    id: ID!
  ): Song

  songs(
    filterBy: SongsFilterBy
    from: Int = 0
    limit: Int = 10
    sortBy: [SongsSortBy!]
  ): Songs
`

export const mutations = `
  createSong(
    name: String!
    sources: SourcesInput = {}
  ): Song @auth

  updateSong(
    id: ID!
    name: String
    sources: SourcesInput = {}
  ): Song @auth

  deleteSong(
    id: ID!
  ): Song @auth
`

export const createHandler =
  (async ({
    params: {
      name,
      sources: { youtubeId },
    },
    context: { id },
  }) => {
    try {
      const song = await db.Song.create({ creatorId: id, name, sources: { youtubeId } })

      return song
    } catch (error) {
      throw error
    }
  })
  |> validation(
    async ({
      params: {
        name,
        sources: { youtubeId },
      },
    }) => await validations.songs.create({ name, sources: { youtubeId } }),
  )

export const allHandler = async ({ params: { filterBy, from, limit, sortBy } }) => {
  try {
    const { items, count, total } = await db.Song.find({ filterBy, from, limit, sortBy })

    return { items, count, total }
  } catch (error) {
    throw error
  }
}

export const updateHandler =
  (async ({
    params: {
      id,
      name,
      sources: { youtubeId },
    },
  }) => {
    try {
      const song = await db.Song.updateById(id, { name, sources: { youtubeId } })

      return song
    } catch (error) {
      throw error
    }
  })
  |> validation(
    async ({
      params: {
        name,
        sources: { youtubeId },
      },
    }) => await validations.songs.update({ name, sources: { youtubeId } }),
  )

// export const deleteHandler = async ({
//   params: {
//     id,
//     name,
//     sources: { youtubeId },
//   },
// }) => {
//   try {
//     const song = await db.deleteSong(id, { name, sources: { youtubeId } })

//     return song
//   } catch (error) {
//     log.error(error)

//     if (isAllowedError(error)) {
//       throw error
//     }

//     throw new errors.HandlerError(error)
//   }
// }

export default {
  types,
  queries,
  mutations,
  createHandler,
  allHandler,
  updateHandler,
  // delete: deleteHandler,
}
