import db from '@/db'

export const types = `
  type PlaylistSong {
    id: ID!
    playlistId: ID!
    songId: ID!
    createdAt: Date!
    updatedAt: Date!
    deletedAt: Date
  }
`

export const queries = `
  playlistSongs(
    playlistId: ID!
    limit: Int
    offset: Int
  ): Songs!
`

export const mutations = `
  createPlaylistSong(
    playlistId: ID!
    songId: ID!
  ): PlaylistSong

  deletePlaylistSong(
    playlistId: ID!
    songId: ID!
  ): PlaylistSong
`

export const createHandler = async ({ params: { playlistId, songId } }) => {
  try {
    const playlist = await db.PlaylistSong.create({
      playlistId,
      songId,
    })

    return playlist
  } catch (error) {
    throw error
  }
}

export default {
  types,
  queries,
  mutations,
  createHandler,
}
