import google from '@/api/google'
import tokens from '@/services/tokens'
import db from '@/db'

export const types = `
  type Auth {
    token: String!
    googleToken: String!
  }
`

export const queries = ''

export const mutations = `
  login(
    code: String!
    redirectUrl: String!
  ): Auth
`

export const loginHandler = async (root, { code, redirectUrl }) => {
  try {
    const { token: googleToken, refreshToken: googleRefreshToken } = await google.createToken({
      code,
      redirectUrl,
    })

    const { email } = await google.getAccount()

    const user = await db.users.upsert({ email })

    const token = tokens.create({ _id: user._id })

    return {
      token,
      googleToken,
      googleRefreshToken,
    }
  } catch (error) {
    throw error
  }
}

export default {
  types,
  queries,
  mutations,
  loginHandler,
}
