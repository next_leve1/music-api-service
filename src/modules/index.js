import { gql } from 'apollo-server-lambda'

import definitionsSchema from './schema/definitions'
import auth from './auth'
import users from './users'
import songs from './songs'
import * as playlists from './playlists'
import playlistSongs from './playlist-songs'
import AuthDirective from './schema/directives/auth'

export const typesSchema = `
  ${auth.types}
  ${users.types}
  ${playlists.types}
`

export const queriesSchema = `
  type Query {
    ${auth.queries}
    ${users.queries}
    ${playlists.queries}
  }
`

export const mutationsSchema = `
  type Mutation {
    ${auth.mutations}
    ${users.mutations}
    ${playlists.mutations}
  }
`

export const schema = `
  ${definitionsSchema}
  ${typesSchema}
  ${queriesSchema}
  ${mutationsSchema}
`

export const Query = {
  getUser: users.getHandler,
  playlist: playlists.getHandler,
  // songs: songs.allHandler,
}

export const Mutation = {
  login: auth.loginHandler,
  // updateUser: users.updateHandler,
  removeUser: users.removeHandler,
  // createSong: songs.createHandler,
  // updateSong: songs.updateHandler,
  createPlaylist: playlists.createHandler,
  updatePlaylist: playlists.updateHandler,
  removePlaylist: playlists.removeHandler,
}

export const typeDefs = gql`
  ${schema}
`

export const resolvers = {
  Query,
  Mutation,
}

export const schemaDirectives = {
  auth: AuthDirective,
  // validation: ValidationDirective,
}

export default {
  typesSchema,
  queriesSchema,
  mutationsSchema,
  schema,
  typeDefs,
  resolvers,
  schemaDirectives,
}
