import { AuthenticationError } from 'apollo-server-lambda'
import db from '@/db'
// import validation from './utils/validation'
// import validations from '@/validations'

export const types = `
  type User {
    _id: ID!
    name: String
    email: String!
    createdAt: Date!
    updatedAt: Date!
    deletedAt: Date
  }
`

export const queries = `
  getUser(_id: ID): User @auth
`

export const mutations = `
  updateUser(name: String): User @auth

  removeUser: User @auth
`

export const getHandler = async (parent, params, { _id }) => {
  try {
    const user = await db.users.get(_id)

    if (!user) {
      throw new AuthenticationError('User not found')
    }

    return user
  } catch (error) {
    throw error
  }
}

// export const updateHandler =
//   (async (parent, { name }, { _id }) => {
//     try {
//       const user = await db.users.updateById(_id, { name })

//       return user
//     } catch (error) {
//       throw error
//     }
//   })
//   |> validation(
//     async (parent, { name }, { _id }) => await validations.users.update({ name }, { _id }),
//   )

export const removeHandler = async (parent, params, { _id }) => {
  try {
    const user = await db.users.remove(_id)

    return user
  } catch (error) {
    throw error
  }
}

export default {
  types,
  queries,
  mutations,
  getHandler,
  // updateHandler,
  removeHandler,
}
