import server from './server'

export default server.apolloServer.createHandler({
  cors: {
    origin: 'http://localhost:3000',
    credentials: true,
  },
})
