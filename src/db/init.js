import db from './instance'
import createUsers from './users'

const DEVELOPMENT_DB_NAME = 'development'
const TEST_DB_NAME = 'test'

const init = async () => {
  await db.dropDatabase(TEST_DB_NAME)

  await db.createDatabase(TEST_DB_NAME)

  db.useDatabase(TEST_DB_NAME)

  const users = db.collection('users')

  await users.create()

  const playlists = db.collection('playlists')

  await playlists.create()
}

init()
