import { aql } from 'arangojs'

import db from './instance'

export const insert = async ({ email }) => {
  const cursor = await db.query(aql`
    INSERT {
      email: ${email},
      createdAt: DATE_NOW(),
      updatedAt: DATE_NOW()
    }
    IN users
    RETURN NEW
  `)

  return await cursor.next()
}

export const upsert = async ({ email }) => {
  const cursor = await db.query(aql`
    UPSERT { email: ${email} }
    INSERT {
      email: ${email},
      createdAt: DATE_NOW(),
      updatedAt: DATE_NOW()
    }
    UPDATE {
      updatedAt: DATE_NOW()
    }
    IN users
    RETURN NEW
  `)

  return await cursor.next()
}

export const get = async (param) => {
  if (typeof param === 'string') {
    const _id = param

    const cursor = await db.query(aql`RETURN DOCUMENT(${_id})`)

    return await cursor.next()
  }

  const { _id = null, email = null } = param

  const cursor = await db.query(aql`
    FOR user IN users
      FILTER
        user._id == ${_id} OR user.email == ${email}
      RETURN user
  `)

  return await cursor.next()
}

export const remove = async (_id) => {
  const cursor = await db.query(aql`REMOVE DOCUMENT(${_id}) IN users`)

  return await cursor.next()
}

export const removeAll = async () => {
  const cursor = await db.query(aql`
    FOR user IN users
      REMOVE user IN users
  `)

  return await cursor.next()
}

export default {
  insert,
  upsert,
  get,
  remove,
  removeAll,
}
