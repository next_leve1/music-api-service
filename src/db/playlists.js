import { aql } from 'arangojs'

import db from './instance'

export const exists = async (param) => {
  const playlist = await get(param)

  const isExists = !!playlist

  return isExists
}

export const insert = async ({ _ownerId, name }) => {
  const cursor = await db.query(aql`
    INSERT {
      _ownerId: ${_ownerId},
      name: ${name},
      createdAt: DATE_NOW(),
      updatedAt: DATE_NOW()
    }
    IN playlists
    RETURN NEW
  `)

  return await cursor.next()
}

export const get = async (param) => {
  if (typeof param === 'string') {
    const _id = param

    const cursor = await db.query(aql`RETURN DOCUMENT(${_id})`)

    return await cursor.next()
  }

  const { _id = null, _ownerId = null, name = null } = param

  const cursor = await db.query(aql`
    FOR playlist IN playlists
      FILTER
        playlist._id == ${_id} OR playlist._ownerId == ${_ownerId} OR playlist.name == ${name}
      RETURN playlist
  `)

  return await cursor.next()
}

export const update = async (_id, { name }) => {
  const cursor = await db.query(aql`
    UPDATE DOCUMENT(${_id}) WITH {
      name: ${name},
      updatedAt: DATE_NOW()
    } IN playlists
    RETURN NEW
  `)

  return await cursor.next()
}

export const upsert = async ({ name, _ownerId }) => {
  const cursor = await db.query(aql`
    UPSERT { name: ${name} }
    INSERT {
      _ownerId: ${_ownerId},
      name: ${name},
      createdAt: DATE_NOW(),
      updatedAt: DATE_NOW()
    }
    UPDATE {
      updatedAt: DATE_NOW()
    }
    IN playlists
    RETURN NEW
  `)

  return await cursor.next()
}

export const remove = async (_id) => {
  const cursor = await db.query(aql`
    REMOVE DOCUMENT(${_id}) IN playlists
  `)

  return await cursor.next()
}

export const removeAll = async () => {
  const cursor = await db.query(aql`
    FOR playlist IN playlists
      REMOVE playlist IN playlists
  `)

  return await cursor.next()
}
