import validation from '@next_leve1/validation'
import validators from './utils/validators'

export const update = validation.create({
  name: [validators.maxLength(50), validators.same, validators.notAvailable],
})

export default {
  update,
}
