import validation from '@next_leve1/validation'
import validators from './utils/validators'

export const create = validation.create({
  name: [validators.required, validators.minLength(5), validators.maxLength(80)],
  sources: {
    youtubeId: [validators.maxLength(80)],
  },
})

export const update = validation.create({
  name: [validators.minLength(5), validators.maxLength(80)],
  sources: {
    youtubeId: [validators.maxLength(80)],
  },
})

export default {
  create,
  update,
}
