import users from './users'
import songs from './songs'

export default {
  users,
  songs,
}
