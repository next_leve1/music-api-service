import { Op } from 'sequelize'
import constants from '@/constants'
import db from '@/db'

export const required = (value) => !value && constants.validationMessages.required
export const minLength = (min) => (value) =>
  typeof value === 'string' && value.length < min && constants.validationMessages.minLength(min)
export const maxLength = (max) => (value) =>
  typeof value === 'string' && value.length > max && constants.validationMessages.maxLength(max)
export const same = async (value, { id }) =>
  (await db.User.exists({ where: { id: id, name: value } })) && constants.validationMessages.same
export const notAvailable = async (value, { id }) =>
  (await db.User.exists({ where: { id: { [Op.not]: id }, name: value } })) &&
  constants.validationMessages.notAvailable

export default {
  required,
  minLength,
  maxLength,
  same,
  notAvailable,
}
