import config from '../config'

export const error = (...args) => {
  if (!config.log) return

  console.log('\n\x1b[31m%s\x1b[0m', 'error', ...args, '\n')
}

export const info = (...args) => {
  if (!config.log) return

  console.log('\n\x1b[32m%s\x1b[0m', 'info', ...args, '\n')
}

export default {
  error,
  info,
}
