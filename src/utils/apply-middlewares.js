export default (actions, middlewares) => {
  const middlewaredActions = {}

  Object.keys(actions).forEach((actionKey) => {
    const action = actions[actionKey]

    middlewaredActions[actionKey] = (data) => {
      const result = action(data)

      return middlewares.reduce((reducedResult, middleware) => middleware(reducedResult), result)
    }
  })

  return middlewaredActions
}
