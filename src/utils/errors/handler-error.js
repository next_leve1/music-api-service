import OperationalError from './operational-error'
import constants from '@/constants'

const normalizeInput = (message, additionalProperties) => {
  let normalizedMessage = constants.errors.HANDLERS_ERROR_DEFAULT_MESSAGE
  const normalizedAdditionalProperties = { ...additionalProperties }

  if (message !== null && typeof message === 'object') {
    const error = message

    normalizedMessage = error.message
  } else {
    normalizedMessage = message
  }

  return {
    message: normalizedMessage,
    code: constants.errors.HANDLERS_ERROR_CODE,
    additionalProperties: normalizedAdditionalProperties,
  }
}

export default class HandlerError extends OperationalError {
  constructor(...args) {
    const { message, code, additionalProperties } = normalizeInput(...args)

    super(message, code, additionalProperties)
  }
}
