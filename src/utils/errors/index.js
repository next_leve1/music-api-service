import OperationalError from './operational-error'
import ServerError from './server-error'
import HandlerError from './handler-error'
import ValidationError from './validation-error'
import AuthenticationError from './authentication-error'
import GoogleAPIsError from './google-apis-error'

export {
  OperationalError,
  ServerError,
  HandlerError,
  ValidationError,
  AuthenticationError,
  GoogleAPIsError,
}

export default {
  OperationalError,
  ServerError,
  HandlerError,
  ValidationError,
  AuthenticationError,
  GoogleAPIsError,
}
