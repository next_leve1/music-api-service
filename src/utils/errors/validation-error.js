import OperationalError from './operational-error'
import constants from '@/constants'

const normalizeInput = (invalid, additionalProperties) => {
  const normalizedAdditionalProperties = { ...additionalProperties, invalid }

  return {
    message: constants.errors.VALIDATION_ERROR_DEFAULT_MESSAGE,
    code: constants.errors.VALIDATION_ERROR_CODE,
    additionalProperties: normalizedAdditionalProperties,
  }
}

export default class ValidationError extends OperationalError {
  constructor(...args) {
    const { message, code, additionalProperties } = normalizeInput(...args)

    super(message, code, additionalProperties)
  }
}
