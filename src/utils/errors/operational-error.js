import { ApolloError } from 'apollo-server-lambda'
import constants from '@/constants'

const normalizeInput = (message, code, additionalProperties) => {
  let normalizedAdditionalProperties = {}
  if (typeof additionalProperties === 'object') {
    normalizedAdditionalProperties = additionalProperties
  } else if (typeof code === 'object') {
    normalizedAdditionalProperties = code
  } else if (typeof message === 'object') {
    normalizedAdditionalProperties = message
  }

  let normalizedCode = constants.errors.OPERATIONAL_ERROR_CODE
  if (typeof code === 'string' && constants.errors.codes.includes(code)) {
    normalizedCode = code
  } else if (typeof message === 'string' && constants.errors.codes.includes(message)) {
    normalizedCode = message
  }

  let normalizedMessage = constants.errors.OPERATIONAL_ERROR_DEFAULT_MESSAGE
  if (typeof message === 'string') {
    normalizedMessage = message
  }

  return {
    message: normalizedMessage,
    code: normalizedCode,
    additionalProperties: normalizedAdditionalProperties,
  }
}

export default class OperationalError extends ApolloError {
  constructor(...args) {
    const { message, code, additionalProperties } = normalizeInput(...args)

    super(message, code, additionalProperties)
  }
}
