import OperationalError from './operational-error'
import constants from '@/constants'

const normalizeInput = (message, additionalProperties) => {
  let normalizedMessage = constants.errors.GOOGLE_APIS_ERROR_DEFAULT_MESSAGE
  const normalizedAdditionalProperties = { ...additionalProperties }

  if (message !== null && typeof message === 'object') {
    const error = message

    if (error.response && error.response.data) {
      const {
        response: {
          data: { error: originalCode, error_description: originalMessage },
        },
      } = error

      Object.assign(normalizedAdditionalProperties, {
        originalCode,
        originalMessage,
      })
    } else {
      normalizedMessage = error.message
    }
  } else {
    normalizedMessage = message
  }

  return {
    message: normalizedMessage,
    code: constants.errors.GOOGLE_APIS_ERROR_CODE,
    additionalProperties: normalizedAdditionalProperties,
  }
}

export default class GoogleAPIsError extends OperationalError {
  constructor(...args) {
    const { message, code, additionalProperties } = normalizeInput(...args)

    super(message, code, additionalProperties)
  }
}
