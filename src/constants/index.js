import validationMessages from './validation-messages'
import errors from './errors'

export default {
  validationMessages,
  errors,
}
