export const string = 'Should be a string'
export const required = 'Is required'
export const minLength = (length) => `Should be at least ${length} characters long`
export const maxLength = (length) => `Should be less than or equal to ${length} characters long`
export const same = 'Is the same'
export const notAvailable = 'Is not available'

export default {
  string,
  required,
  minLength,
  maxLength,
  same,
  notAvailable,
}
