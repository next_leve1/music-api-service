const webpack = require('webpack')
const serverlessWebpack = require('serverless-webpack')
const webpackNodeExternals = require('webpack-node-externals')

const nodeExternals = webpackNodeExternals()

module.exports = {
  target: 'node',
  entry: serverlessWebpack.lib.entries,
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
      },
    ],
  },
  mode: process.env.NODE_ENV !== 'test' ? process.env.NODE_ENV : 'none',
  externals: [nodeExternals],
  plugins: [
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
    }),
  ],
}
