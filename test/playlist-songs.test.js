import db from '@/db'
import tokens from '@/services/tokens'
import constants from '@/constants'
import queries from './utils/queries'
import fake from './fake'

describe('playlist schema', () => {
  let creatorId
  let token

  before(async () => {
    const fakeUser = fake.user()
    const dbUser = await db.User.upsert(fakeUser)

    creatorId = dbUser.id
    token = tokens.create({ id: creatorId })
  })

  after(async () => {
    await db.User.deleteAll()
  })

  beforeEach(async () => {
    await db.PlaylistSong.deleteAll()
  })

  it('add song to a playlist', async () => {
    const fakePlaylist = fake.playlist({ creatorId })
    const fakeSong = fake.song({ creatorId })

    const dbPlaylist = await db.Playlist.create(fakePlaylist)
    const dbSong = await db.Song.create(fakeSong)

    const playlistSongData = { playlistId: dbPlaylist.id, songId: dbSong.id }

    const { createPlaylistSong: playlistSong } = await request(
      queries.createPlaylistSong(playlistSongData),
      {
        headers: { token },
      },
    )

    console.log(playlistSong, playlistSongData)

    assert.equal(playlistSong.playlistId, playlistSongData.playlistId)
    assert.equal(playlistSong.songId, playlistSongData.songId)
  })
})
