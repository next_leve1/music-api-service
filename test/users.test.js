import db from '@/db'
import tokens from '@/services/tokens'
import queries from './utils/queries'
import fake from './fake'

describe('users schema', () => {
  beforeEach(async () => {
    await db.users.removeAll()
  })

  it('getUser successfull', async () => {
    const fakeUser = fake.user()

    const dbUser = await db.users.insert(fakeUser)

    const token = tokens.create({ _id: dbUser._id })

    const { getUser: user } = await request(queries.getUser(), { headers: { token } })

    assert.isOk(user._id)
    // assert.equal(user.name, fakeUser.name)
    assert.equal(user.email, fakeUser.email)
    assert.isTrue(check.isTime(user.createdAt))
    assert.isTrue(check.isTime(user.updatedAt))
    // assert.isNull(user.deletedAt)
  })

  it('getUser user not found', async () => {
    try {
      const token = tokens.create({ _id: -1 })

      await request(queries.getUser(), { headers: { token } })

      assert.fail()
    } catch (error) {
      assert.isObject(error[0])
      assert.equal(error[0].extensions.code, 'UNAUTHENTICATED')
    }
  })

  it('getUser invalid token', async () => {
    try {
      await request(queries.getUser(), { headers: { token: fake.invalidToken() } })

      assert.fail()
    } catch (error) {
      assert.isObject(error[0])
      assert.equal(error[0].extensions.code, 'UNAUTHENTICATED')
    }
  })

  it('getUser without token', async () => {
    try {
      await request(queries.getUser())

      assert.fail()
    } catch (error) {
      assert.isObject(error[0])
      assert.equal(error[0].extensions.code, 'UNAUTHENTICATED')
    }
  })

  // it('update user successfull', async () => {
  //   const dbUser = await db.users.create({ ...fake.user(), name: null })

  //   const token = tokens.create({ _id: dbUser._id })

  //   const fakeName = fake.name()

  //   const { updateUser: user } = await request(queries.updateUser({ name: fakeName }), {
  //     headers: { token },
  //   })

  //   assert.isOk(user._id)
  //   // assert.equal(user.name, fakeName)
  //   assert.isTrue(check.isTime(user.createdAt))
  //   assert.isTrue(check.isTime(user.updatedAt))
  //   assert.notEqual(dbUser.updatedAt, user.updatedAt)
  // })

  // it('update user empty name', async () => {
  //   const dbUser = await db.users.create({
  //     ...fake.user(),
  //     name: null,
  //   })

  //   const token = tokens.create({ _id: dbUser._id })

  //   const fakeName = fake.emptyName()

  //   const { updateUser: user } = await request(queries.updateUser({ name: fakeName }), {
  //     headers: { token },
  //   })

  //   const updatedDbUser = await db.users.findById(dbUser._id)

  //   assert.equal(user.name, fakeName)
  //   assert.equal(updatedDbUser.name, fakeName)
  // })

  // it('update user invalid name max length', async () => {
  //   const fakeUser = fake.user()
  //   const dbUser = await db.users.create(fakeUser)

  //   const token = tokens.create({ _id: dbUser._id })

  //   try {
  //     await request(queries.updateUser({ name: fake.longerName() }), { headers: { token } })

  //     assert.fail()
  //   } catch (error) {
  //     assert.isString(error[0].code)
  //     assert.equal(error[0].code, constants.errors.VALIDATION_ERROR_CODE)
  //     assert.equal(error[0].invalid.name[0], constants.validationMessages.maxLength(50))
  //   }
  // })

  // it('update user with the same name', async () => {
  //   const fakeUser = fake.user()
  //   const dbUser = await db.users.create(fakeUser)

  //   const token = tokens.create({ _id: dbUser._id })

  //   try {
  //     await request(queries.updateUser({ name: fakeUser.name }), { headers: { token } })

  //     assert.fail()
  //   } catch (error) {
  //     assert.isString(error[0].code)
  //     assert.equal(error[0].code, constants.errors.VALIDATION_ERROR_CODE)
  //     assert.equal(error[0].invalid.name[0], constants.validationMessages.same)
  //   }
  // })

  // it('update user invalid name not unique', async () => {
  //   const fakeUser = fake.user()
  //   const anotherFakeUser = fake.user()
  //   const dbUser = await db.users.create(fakeUser)
  //   const anotherDbUser = await db.users.create(anotherFakeUser)

  //   const token = tokens.create({ _id: anotherDbUser._id })

  //   try {
  //     await request(queries.updateUser({ name: dbUser.name }), { headers: { token } })

  //     assert.fail()
  //   } catch (error) {
  //     assert.isString(error[0].code)
  //     assert.equal(error[0].code, constants.errors.VALIDATION_ERROR_CODE)
  //     assert.equal(error[0].invalid.name[0], constants.validationMessages.notAvailable)
  //   }
  // })

  // it('update undefined user', async () => {
  //   const token = tokens.create({ _id: -1 })

  //   const { updateUser: user } = await request(queries.updateUser({ name: fake.name() }), {
  //     headers: { token },
  //   })

  //   assert.isNull(user)
  // })

  // it('update user with bad credentials', async () => {
  //   try {
  //     await request(queries.updateUser({ name: fake.name() }), {
  //       headers: { token: fake.invalidToken() },
  //     })

  //     assert.fail()
  //   } catch (error) {
  //     assert.isString(error[0].code)
  //     assert.equal(error[0].code, constants.errors.AUTHENTICATION_ERROR_CODE)
  //   }
  // })

  it('removeUser successfull', async () => {
    const fakeUser = fake.user()
    const dbUser = await db.users.insert(fakeUser)

    const token = tokens.create({ _id: dbUser._id })

    const { removeUser: user } = await request(queries.removeUser(), { headers: { token } })

    assert.isNull(user)
  })

  // it('removeUser that undefined', async () => {
  //   const token = tokens.create()

  //   const { removeUser: user } = await request(queries.removeUser(), { headers: { token } })

  //   assert.isNull(user)
  // })

  it('removeUser with bad credentials', async () => {
    try {
      await request(queries.removeUser(), {
        headers: { token: fake.invalidToken() },
      })

      assert.fail()
    } catch (error) {
      assert.isObject(error[0])
      assert.equal(error[0].extensions.code, 'UNAUTHENTICATED')
    }
  })
})
