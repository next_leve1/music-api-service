import nock from 'nock'

import { baseURL as googleBaseURL } from '@/api/google'
import { createTokenSettings, getAccountSettings } from '@/api/google'
import db from '@/db'
import tokens from '@/services/tokens'
import queries from './utils/queries'
import fake from './fake'

describe('auth schema', () => {
  beforeEach(async () => {
    await db.users.removeAll()
  })

  it('login successfull', async () => {
    const email = fake.email()
    const code = fake.code()
    const redirectUrl = fake.redirectUrl()
    const token = fake.token()
    const refreshToken = fake.refreshToken()

    nock(googleBaseURL, {
      reqheaders: createTokenSettings.headers, // FIXME: No reaction
    })
      .post(createTokenSettings.path)
      .query(
        createTokenSettings.params({
          code,
          redirectUrl,
        }),
      )
      .reply(200, { access_token: token, refresh_token: refreshToken })

    nock(getAccountSettings.url, {
      reqheaders: {
        authorization: `Bearer ${token}`,
      },
    })
      .get(getAccountSettings.path)
      .query(getAccountSettings.params)
      .reply(200, {
        emailAddresses: [{ value: email }],
      })

    const { login: credentials } = await request(queries.login({ code, redirectUrl }))

    assert.isObject(credentials)
    assert.isString(credentials.token)
    assert.isString(credentials.googleToken)

    const tokenPayload = tokens.check(credentials.token)

    assert.isOk(tokenPayload._id)

    const dbUser = await db.users.get({ email })

    assert.isOk(dbUser._id)
    assert.equal(dbUser.email, email)
    assert.isTrue(check.isTime(dbUser.createdAt))
    assert.isTrue(check.isTime(dbUser.updatedAt))

    assert.isOk(tokens.check(credentials.token))
  })

  it('login google auth invalid', async () => {
    const originalCode = fake.googleApiErrorCode()
    const originalMessage = fake.googleApiErrorMessage()

    try {
      const code = fake.code()
      const redirectUrl = fake.redirectUrl()

      nock(googleBaseURL, {
        reqheaders: createTokenSettings.headers, // FIXME: No reaction
      })
        .post(createTokenSettings.path)
        .query(
          createTokenSettings.params({
            code,
            redirectUrl,
          }),
        )
        .reply(400, {
          error: originalCode,
          error_description: originalMessage,
        })

      await request(queries.login({ code, redirectUrl }))

      assert.fail()
    } catch (error) {
      assert.isObject(error[0])
      assert.equal(error[0].extensions.code, 'INTERNAL_SERVER_ERROR')
    }
  })
})
