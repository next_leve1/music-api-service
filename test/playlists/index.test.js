import db from '@/db'
import tokens from '@/services/tokens'
import constants from '@/constants'
import * as queries from '../queries'
import * as fake from '../fake'

describe('playlists schema', () => {
  let _ownerId
  let token

  before(async () => {
    const fakeUser = fake.user()
    const dbUser = await db.users.insert(fakeUser)

    _ownerId = dbUser._id
    token = tokens.create({ _id: _ownerId })
  })

  after(async () => {
    await db.users.removeAll()
  })

  beforeEach(async () => {
    await db.playlists.removeAll()
  })

  it('create a playlist', async () => {
    const fakePlaylist = fake.playlist()

    const { createPlaylist: playlist } = await request(queries.createPlaylist(fakePlaylist), {
      headers: { token },
    })

    assert.isOk(playlist._id)
    assert.equal(playlist._ownerId, _ownerId)
    assert.equal(playlist.name, fakePlaylist.name)
    assert.isTrue(check.isTime(playlist.createdAt))
    assert.isTrue(check.isTime(playlist.updatedAt))
  })

  it('get a playlist', async () => {
    const fakePlaylist = fake.playlist({ _ownerId })

    const dbPlaylist = await db.playlists.insert(fakePlaylist)

    const { playlist } = await request(queries.getPlaylist({ _id: dbPlaylist._id }))

    assert.isOk(playlist._id)
    assert.equal(playlist._ownerId, _ownerId)
    assert.equal(playlist.name, fakePlaylist.name)
    assert.isTrue(check.isTime(playlist.createdAt))
    assert.isTrue(check.isTime(playlist.updatedAt))
  })

  it('update a playlist', async () => {
    const fakePlaylist = fake.playlist({ _ownerId })
    const fakeNewPlaylistName = fake.playlistName()

    const dbPlaylist = await db.playlists.insert(fakePlaylist)

    const { updatePlaylist: playlist } = await request(
      queries.updatePlaylist({ _id: dbPlaylist._id, name: fakeNewPlaylistName }),
      {
        headers: { token },
      },
    )

    assert.isOk(playlist._id)
    assert.equal(playlist._ownerId, _ownerId)
    assert.equal(playlist.name, fakeNewPlaylistName)
    assert.isTrue(check.isTime(playlist.createdAt))
    assert.isTrue(check.isTime(playlist.updatedAt))
    assert.isTrue(playlist.updatedAt > dbPlaylist.updatedAt)
  })

  it('remove a playlist', async () => {
    const fakePlaylist = fake.playlist({ _ownerId })

    const dbPlaylist = await db.playlists.insert(fakePlaylist)

    await request(queries.removePlaylist({ _id: dbPlaylist._id }), {
      headers: { token },
    })

    assert.isNotOk(await db.playlists.exists({ _id: dbPlaylist._id }))
  })

  // it('playlist add songs', async () => {
  //   const fakePlaylist = fake.playlist({ creatorId })
  //   const fakeSongs = fake.songs(15, { creatorId })

  //   const dbPlaylist = await db.Playlist.create(fakePlaylist)

  //   const { addPlaylistSongs: playlistSongs } = await request(
  //     queries.addPlaylistSongs({ playlistId: dbPlaylist.id, songs: fakeSongs }),
  //     { headers: { token } },
  //   )

  //   assert.isOk(playlist.id)
  //   // assert.equal(playlist.creatorId, creatorId)
  //   assert.equal(playlist.name, fakePlaylist.name)
  //   // assert.isArray(playlist.songs)
  //   // assert.equal(playlist.songs.length, 0)
  //   assert.isTrue(check.isTime(playlist.createdAt))
  //   assert.isTrue(check.isTime(playlist.updatedAt))
  // })
})
