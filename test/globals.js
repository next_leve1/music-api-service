import chai from 'chai'
import request from './utils/request'
import check from './utils/check'

Object.assign(global, {
  assert: chai.assert,
  request,
  check,
})
