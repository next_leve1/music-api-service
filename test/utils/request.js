import { ApolloServer } from 'apollo-server-lambda'
import { createTestClient } from 'apollo-server-testing'
import server from '@/server'

export default async (query, options = {}) => {
  const apolloServer = new ApolloServer({
    typeDefs: server.typeDefs,
    schemaDirectives: server.schemaDirectives,
    resolvers: server.resolvers,
    context: server.context.bind(null, {
      event: {
        headers: options.headers || {},
      },
    }),
  })

  const { query: handleQuery } = createTestClient(apolloServer)

  try {
    const { errors: error, data } = await handleQuery({
      query,
    })

    if (error) {
      throw error
    } else {
      return data
    }
  } catch (error) {
    throw error
  }
}
