export const isoDate = (date) => {
  const isoRegxp = /(\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d\.\d+)|(\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d)|(\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d)/

  return isoRegxp.test(date)
}

export const isTime = (time) => {
  return typeof time === 'number'
}

export default {
  isoDate,
  isTime,
}
