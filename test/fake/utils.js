export const resolveProp = ([key, value]) => {
  if (typeof value === 'object') {
    return [key, resolveAdditionalProps(value)]
  }

  return [key, value]
}

const resolveAdditionalPropsArguments = (additionalProperties) => {
  let resolveAdditionalProperties
  if (typeof additionalProperties === 'function') {
    resolveAdditionalProperties = additionalProperties()
  } else if (additionalProperties) {
    resolveAdditionalProperties = additionalProperties
  } else {
    resolveAdditionalProperties = {}
  }

  return [resolveAdditionalProperties]
}

export const resolveAdditionalProps = (...args) => {
  const [additionalProperties] = resolveAdditionalPropsArguments(...args)

  return Object.fromEntries(
    Object.entries(additionalProperties).map(([key, value]) => {
      if (typeof value === 'function') {
        return resolveProp([key, value()])
      }

      return resolveProp([key, value])
    }),
  )
}

export default {
  resolveProp,
  resolveAdditionalProps,
}
