import faker from 'faker'
import _orderBy from 'lodash.orderby'
import utils from './utils'

export const code = () => faker.random.uuid()
export const redirectUrl = () => 'http://localhost:3000/google-oauth'
export const invalidRedirectUrl = () => 'http://localhost:3234/google-oauth'
export const invalidCode = () => faker.random.uuid()
export const token = () => faker.random.uuid()
export const invalidToken = () => faker.random.uuid()
export const refreshToken = () => faker.random.uuid()

export const email = () => faker.internet.email()
export const name = () => faker.internet.userName()
export const user = () => ({
  email: email(),
  name: name(),
})
export const emptyName = () => ''
export const longerName = () => faker.lorem.paragraph()

export const googleApiErrorCode = () => faker.lorem.word()
export const googleApiErrorMessage = () => faker.lorem.paragraph()

export const songName = () => faker.random.words()
export const youtubeId = () => faker.random.uuid()
export const sources = () => ({ youtubeId: youtubeId() })
export const song = (additionalProperties) => ({
  name: songName(),
  sources: sources(),
  ...utils.resolveAdditionalProps(additionalProperties),
})
export const songs = (count, additionalProperties) =>
  Array.from({ length: count }, () => song(additionalProperties))
export const songNameRequired = () => ''
export const songNameMin = () => 'abcd'
export const songNameMax = () => faker.lorem.paragraphs()
export const youtubeIdMax = () => faker.lorem.paragraphs()

export const playlistName = () => faker.random.words()
export const playlist = (additionalProperties) => ({
  name: faker.random.words(),
  ...utils.resolveAdditionalProps(additionalProperties),
})

export const normalizeSortBy = (sortBy) =>
  sortBy.reverse().reduce(
    (result, { field, direction = 'ASC' }) => {
      const fieldNormalizer = (item) => item[field].toLowerCase()

      result.fields.push(fieldNormalizer)
      result.directions.push(direction)

      return result
    },
    {
      fields: [],
      directions: [],
    },
  )

export const findResult = (fakeItems, { filterBy, from = 0, limit = 10, sortBy } = {}) => {
  let totalItems = fakeItems

  if (filterBy) {
    totalItems = Object.entries(filterBy).reduce(
      (reducedTotalItems, [field, value]) => fakeItems.filter((item) => item[field] === value),
      totalItems,
    )
  }

  if (sortBy) {
    const normalizedSortBy = normalizeSortBy(sortBy)

    totalItems = _orderBy(totalItems, normalizedSortBy.fields, normalizedSortBy.directions)
  }

  const items = totalItems.slice(from, from + limit)
  const { length: total } = totalItems
  const count = total <= limit ? total : limit

  return {
    items,
    total,
    count,
  }
}
