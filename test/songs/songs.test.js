import _sample from 'lodash.sample'
import db from '@/db'
import tokens from '@/services/tokens'
import queries from '../utils/queries'
import fake from '../fake'

const createDbSongs = async (total, additionalProperties) => {
  const fakeSongs = fake.songs(total, additionalProperties)

  const dbSongs = []

  await fakeSongs.reduce(async (promisesChain, song) => {
    await promisesChain

    const dbSong = await db.Song.create(song)

    dbSongs.push(dbSong)
  }, Promise.resolve())

  return dbSongs
}

describe('songs collection', () => {
  let creatorId
  let anotherCreatorId
  let token
  let dbSongs

  before(async () => {
    const fakeUser = fake.user()
    const fakeAnotherUser = fake.user()
    const dbUser = await db.User.upsert(fakeUser)
    const anotherDbUser = await db.User.upsert(fakeAnotherUser)

    creatorId = dbUser.id
    anotherCreatorId = anotherDbUser.id

    token = tokens.create({ id: creatorId })
  })

  after(async () => {
    await db.User.deleteAll()
  })

  beforeEach(async () => {
    await db.Song.deleteAll()

    dbSongs = await createDbSongs(50, () => ({
      creatorId: () => _sample([creatorId, anotherCreatorId]),
    }))
  })

  it('songs witout paramethers', async () => {
    const { items: itemsShouldBe, total: totalShouldBe, count: countShouldBe } = fake.findResult(
      dbSongs,
    )

    const {
      songs: { items, count, total },
    } = await request(queries.songs(), { headers: { token } })

    assert.isArray(items)

    itemsShouldBe.forEach((fakeSong, index) => {
      const { [index]: song } = items

      assert.isOk(song.id)
      assert.equal(song.creatorId, fakeSong.creatorId)
      assert.equal(song.name, fakeSong.name)
      assert.isObject(song.sources)
      assert.equal(song.sources.youtubeId, fakeSong.sources.youtubeId)
      assert.isTrue(check.isTime(song.createdAt))
      assert.isTrue(check.isTime(song.updatedAt))
    })
    assert.equal(total, totalShouldBe)
    assert.equal(count, countShouldBe)
  })

  it('songs with limit and from', async () => {
    const params = { from: 10 }

    const { items: itemsShouldBe, total: totalShouldBe, count: countShouldBe } = fake.findResult(
      dbSongs,
      params,
    )

    const {
      songs: { items, count, total },
    } = await request(queries.songs(params), { headers: { token } })

    assert.isArray(items)

    itemsShouldBe.forEach((fakeSong, index) => {
      const { [index]: song } = items

      assert.isOk(song.id)
      assert.equal(song.creatorId, fakeSong.creatorId)
      assert.equal(song.name, fakeSong.name)
      assert.isObject(song.sources)
      assert.equal(song.sources.youtubeId, fakeSong.sources.youtubeId)
      assert.isTrue(check.isTime(song.createdAt))
      assert.isTrue(check.isTime(song.updatedAt))
    })
    assert.equal(total, totalShouldBe)
    assert.equal(count, countShouldBe)
  })

  it('songs by creator id', async () => {
    const params = { filterBy: { creatorId } }

    const { items: itemsShouldBe, total: totalShouldBe, count: countShouldBe } = fake.findResult(
      dbSongs,
      params,
    )

    const {
      songs: { items, count, total },
    } = await request(queries.songs(params), { headers: { token } })

    assert.isArray(items)

    itemsShouldBe.forEach((fakeSong, index) => {
      const { [index]: song } = items

      assert.isOk(song.id)
      assert.equal(song.creatorId, fakeSong.creatorId)
      assert.equal(song.name, fakeSong.name)
      assert.isObject(song.sources)
      assert.equal(song.sources.youtubeId, fakeSong.sources.youtubeId)
      assert.isTrue(check.isTime(song.createdAt))
      assert.isTrue(check.isTime(song.updatedAt))
    })
    assert.equal(total, totalShouldBe)
    assert.equal(count, countShouldBe)
  })

  it('songs by creator id and sort by name', async () => {
    const params = { filterBy: { creatorId }, sortBy: [{ field: 'name' }] }

    const { items: itemsShouldBe, total: totalShouldBe, count: countShouldBe } = fake.findResult(
      dbSongs,
      params,
    )

    const {
      songs: { items, count, total },
    } = await request(queries.songs(params), { headers: { token } })

    assert.isArray(items)

    itemsShouldBe.forEach((fakeSong, index) => {
      const { [index]: song } = items

      assert.isOk(song.id)
      assert.equal(song.creatorId, fakeSong.creatorId)
      assert.equal(song.name, fakeSong.name)
      assert.isObject(song.sources)
      assert.equal(song.sources.youtubeId, fakeSong.sources.youtubeId)
      assert.isTrue(check.isTime(song.createdAt))
      assert.isTrue(check.isTime(song.updatedAt))
    })
    assert.equal(total, totalShouldBe)
    assert.equal(count, countShouldBe)
  })

  it('songs by creator id and sort by mutiple paramethers', async () => {
    const params = {
      filterBy: { creatorId },
      sortBy: [{ field: 'name', direction: 'ASC' }, { field: 'createdAt', direction: 'ASC' }],
    }

    const { items: itemsShouldBe, total: totalShouldBe, count: countShouldBe } = fake.findResult(
      dbSongs,
      params,
    )

    const {
      songs: { items, count, total },
    } = await request(queries.songs(params), { headers: { token } })

    assert.isArray(items)

    itemsShouldBe.forEach((fakeSong, index) => {
      const { [index]: song } = items

      assert.isOk(song.id)
      assert.equal(song.creatorId, fakeSong.creatorId)
      assert.equal(song.name, fakeSong.name)
      assert.isObject(song.sources)
      assert.equal(song.sources.youtubeId, fakeSong.sources.youtubeId)
      assert.isTrue(check.isTime(song.createdAt))
      assert.isTrue(check.isTime(song.updatedAt))
    })
    assert.equal(total, totalShouldBe)
    assert.equal(count, countShouldBe)
  })
})
