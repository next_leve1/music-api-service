import db from '@/db'
import tokens from '@/services/tokens'
import constants from '@/constants'
import queries from '../utils/queries'
import fake from '../fake'

describe('songs', () => {
  let creatorId
  let token

  before(async () => {
    const fakeUser = fake.user()
    const dbUser = await db.User.upsert(fakeUser)

    creatorId = dbUser.id
    token = tokens.create({ id: creatorId })
  })

  beforeEach(async () => {
    await db.Song.deleteAll()
  })

  it('create a song with sources', async () => {
    const fakeSong = fake.song()

    const { createSong: song } = await request(queries.createSong(fakeSong), { headers: { token } })

    assert.isOk(song.id)
    assert.equal(song.creatorId, creatorId)
    assert.equal(song.name, fakeSong.name)
    assert.isObject(song.sources)
    assert.equal(song.sources.youtubeId, fakeSong.sources.youtubeId)
    assert.isTrue(check.isTime(song.createdAt))
    assert.isTrue(check.isTime(song.updatedAt))
  })

  it('create a song without sources', async () => {
    const songName = fake.songName()

    const { createSong: song } = await request(queries.createSong({ name: songName }), {
      headers: { token },
    })

    assert.isOk(song.id)
    assert.equal(song.creatorId, creatorId)
    assert.equal(song.name, songName)
    assert.isObject(song.sources)
    assert.isNull(song.sources.youtubeId)
    assert.isTrue(check.isTime(song.createdAt))
    assert.isTrue(check.isTime(song.updatedAt))
  })

  it('create a song validation', async () => {
    try {
      await request(queries.createSong({ name: fake.songNameRequired() }), {
        headers: { token },
      })

      assert.fail()
    } catch (error) {
      assert.isString(error[0].code)
      assert.equal(error[0].code, constants.errors.VALIDATION_ERROR_CODE)
      assert.equal(error[0].invalid.name[0], constants.validationMessages.required)
    }

    try {
      await request(queries.createSong({ name: fake.songNameMin() }), { headers: { token } })

      assert.fail()
    } catch (error) {
      assert.isString(error[0].code)
      assert.equal(error[0].code, constants.errors.VALIDATION_ERROR_CODE)
      assert.equal(error[0].invalid.name[0], constants.validationMessages.minLength(5))
    }

    try {
      await request(
        queries.createSong({
          name: fake.songNameMax(),
          sources: { youtubeId: fake.youtubeIdMax() },
        }),
        { headers: { token } },
      )

      assert.fail()
    } catch (error) {
      assert.isString(error[0].code)
      assert.equal(error[0].code, constants.errors.VALIDATION_ERROR_CODE)
      assert.equal(error[0].invalid.name[0], constants.validationMessages.maxLength(80))
      assert.equal(
        error[0].invalid.sources.youtubeId[0],
        constants.validationMessages.maxLength(80),
      )
    }
  })

  it('update a song with name and sources', async () => {
    const fakeSong = fake.song({ creatorId })
    const fakeNewSong = fake.song()

    const dbSong = await db.Song.create(fakeSong)

    const { updateSong: song } = await request(
      queries.updateSong({ id: dbSong.id, ...fakeNewSong }),
      { headers: { token } },
    )

    assert.isOk(song.id)
    assert.equal(song.creatorId, creatorId)
    assert.equal(song.name, fakeNewSong.name)
    assert.isObject(song.sources)
    assert.equal(song.sources.youtubeId, fakeNewSong.sources.youtubeId)
    assert.isTrue(check.isTime(song.createdAt))
    assert.isTrue(check.isTime(song.updatedAt))
    assert.notEqual(dbSong.updatedAt, song.updatedAt)
  })

  it('update a song validation', async () => {
    const fakeSong = fake.song({ creatorId })

    const dbSong = await db.Song.create(fakeSong)

    try {
      await request(
        queries.updateSong({
          id: dbSong.id,
          name: fake.songNameMin(),
        }),
        { headers: { token } },
      )

      assert.fail()
    } catch (error) {
      assert.isString(error[0].code)
      assert.equal(error[0].code, constants.errors.VALIDATION_ERROR_CODE)
      assert.equal(error[0].invalid.name[0], constants.validationMessages.minLength(5))
    }

    try {
      await request(
        queries.updateSong({
          id: dbSong.id,
          name: fake.songNameMax(),
          sources: { youtubeId: fake.youtubeIdMax() },
        }),
        { headers: { token } },
      )

      assert.fail()
    } catch (error) {
      assert.isString(error[0].code)
      assert.equal(error[0].code, constants.errors.VALIDATION_ERROR_CODE)
      assert.equal(error[0].invalid.name[0], constants.validationMessages.maxLength(80))
      assert.equal(
        error[0].invalid.sources.youtubeId[0],
        constants.validationMessages.maxLength(80),
      )
    }
  })
})
