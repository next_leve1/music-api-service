import utils from './utils'

export const login = (params) => `
  mutation {
    login${utils.stringifyParams(params)} {
      token
      googleToken
    }
  }
`

export const getUser = () => `
  query {
    getUser {
      _id
      email
      name
      createdAt
      updatedAt
    }
  }
`

export const updateUser = (params) => `
  mutation {
    updateUser${utils.stringifyParams(params)} {
      _id
      email
      name
      createdAt
      updatedAt
      deletedAt
    }
  }
`

export const removeUser = () => `
  mutation {
    removeUser {
      _id
      email
      name
      createdAt
      updatedAt
      deletedAt
    }
  }
`

export const createSong = (params) => `
  mutation {
    createSong${utils.stringifyParams(params)} {
      _id
      creatorId
      name
      sources {
        youtubeId
      }
      createdAt
      updatedAt
      deletedAt
    }
  }
`

export const updateSong = (params) => `
  mutation {
    updateSong${utils.stringifyParams(params)} {
      _id
      creatorId
      name
      sources {
        youtubeId
      }
      createdAt
      updatedAt
      deletedAt
    }
  }
`

export const songs = (params) => `
  query {
    songs${utils.stringifyParams(params)} {
      items {
        id
        creatorId
        name
        sources {
          youtubeId
        }
        createdAt
        updatedAt
        deletedAt
      }
      count
      total
    }
  }
`

export const createPlaylist = (params) => `
  mutation {
    createPlaylist${utils.stringifyParams(params)} {
      _id
      _ownerId
      name
      createdAt
      updatedAt
      deletedAt
    }
  }
`

export const getPlaylist = (params) => `
  query {
    playlist${utils.stringifyParams(params)} {
      _id
      _ownerId
      name
      createdAt
      updatedAt
      deletedAt
    }
  }
`

export const updatePlaylist = (params) => `
  mutation {
    updatePlaylist${utils.stringifyParams(params)} {
      _id
      _ownerId
      name
      createdAt
      updatedAt
      deletedAt
    }
  }
`

export const removePlaylist = (params) => `
  mutation {
    removePlaylist${utils.stringifyParams(params)} {
      _id
      _ownerId
      name
      createdAt
      updatedAt
      deletedAt
    }
  }
`

export const createPlaylistSong = (params) => `
  mutation {
    createPlaylistSong${utils.stringifyParams(params)} {
      id
      playlistId
      songId
      createdAt
      updatedAt
      deletedAt
    }
  }
`
