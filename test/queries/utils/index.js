export const structurizeObject = (params) =>
  `{
    ${Object.entries(params).reduce(
      (result, [paramKey, paramValue]) => `${result}${result ? '\n' : ''}${stringifyParam(paramKey, paramValue)}`,
      '',
    )}
  }`

export const structurizeArray = (paramsArray, key) =>
  `[
    ${paramsArray.reduce(
      (result, paramValue) => `${result}${result ? ',\n' : ''}${stringifyValue(paramValue, key)}`,
      '',
    )}
  ]`

const enums = {
  field: ['creatorId', 'name', 'createdAt', 'updatedAt'],
  direction: ['ASC', 'DESC'],
}

export const structurizeEnum = (value) => `${value}`

export const stringifyValue = (value, key) => {
  for (const [enumKey, enumAllowableValues] of Object.entries(enums)) {
    if (key === enumKey && enumAllowableValues.includes(value)) {
      return structurizeEnum(value)
    }
  }

  if (Array.isArray(value)) {
    return structurizeArray(value, key)
  }

  if (typeof value === 'object') {
    return structurizeObject(value)
  }

  return JSON.stringify(value)
}

export const stringifyParam = (key, value) => `${key}: ${stringifyValue(value, key)}`

export const stringifyParams = (params) => {
  if (typeof params === 'object') {
    const stringifiedParams = `
      ${Object.entries(params).reduce((result, [paramKey, paramValue]) => {
        if (paramValue === undefined) {
          return result
        }

        return `${result}${result ? '\n' : ''}${stringifyParam(paramKey, paramValue)}`
      }, '')}
    `

    if (stringifiedParams) {
      return `(
        ${stringifiedParams}
      )`
    }

    return ''
  }

  return ''
}

export default {
  stringifyParam,
  stringifyParams,
}
